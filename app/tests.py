from django.test import TestCase
from app.models import Bill, Period, Payment, BillsFile, PaymentFile

# Create your tests here for models.
class TestModels(TestCase):
    
    #create test for model Periods
    def test_create_period(self):
        period = Period.objects.create(name='testperiod', date_start='2021-01-01', date_end='2021-01-31', user_id=1)
        self.assertEqual(period.name, 'testperiod')
        self.assertEqual(period.date_start, '2021-01-01')
        self.assertEqual(period.date_end, '2021-01-31')
        self.assertEqual(period.user_id, 1) 
    
    #create test for model Payments
    def test_create_payment(self):
        payment = Payment.objects.create(name='testpayment', amount=1000, date='2021-01-01', user_id=1)
        self.assertEqual(payment.name, 'testpayment')
        self.assertEqual(payment.amount, 1000)
        self.assertEqual(payment.date, '2021-01-01')
        self.assertEqual(payment.user_id, 1)


    #create test for model Bills
    def test_create_bill(self):
        bill = Bill.objects.create(name='testbill', amount=1000, date='2021-01-01', user_id=1)
        self.assertEqual(bill.name, 'testbill')
        self.assertEqual(bill.amount, 1000)
        self.assertEqual(bill.date, '2021-01-01')
        self.assertEqual(bill.user_id, 1)   

# created test for model BillsFile
    def test_create_billsfile(self):
        billsfile = BillsFile.objects.create(name='testbillsfile', file='testbillsfile.pdf', user_id=1)
        self.assertEqual(billsfile.name, 'testbillsfile')
        self.assertEqual(billsfile.file, 'testbillsfile.pdf')
        self.assertEqual(billsfile.user_id, 1)

# created test for model PaymentFile
    def test_create_paymentfile(self):
        paymentfile = PaymentFile.objects.create(name='testpaymentfile', file='testpaymentfile.pdf', user_id=1)
        self.assertEqual(paymentfile.name, 'testpaymentfile')
        self.assertEqual(paymentfile.file, 'testpaymentfile.pdf')
        self.assertEqual(paymentfile.user_id, 1)    



