# Generated by Django 3.1.1 on 2020-09-21 12:07

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_billsfile_paymentfile'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='billsfile',
            options={'verbose_name': 'Laskutiedosto', 'verbose_name_plural': 'Laskutiedostot'},
        ),
        migrations.AlterModelOptions(
            name='paymentfile',
            options={'verbose_name': 'Tiliotte', 'verbose_name_plural': 'Tiliotteet'},
        ),
    ]
