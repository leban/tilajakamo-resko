from django.test import TestCase
from django.contrib.admin.sites import AdminSite
from django.contrib.auth.models import User
from django.utils import timezone
from app.admin import PeriodAdmin
from app.models import Period, Payment, Bill

class PeriodAdminTestCase(TestCase):
    def setUp(self):
        self.site = AdminSite()
        self.admin = PeriodAdmin(Period, self.site)
        self.user = User.objects.create_user(username='testuser', password='12345')
        self.period = Period.objects.create(year=timezone.now().year, month=1)
        self.bill = Bill.objects.create(site='Meilahti', name='Test Bill', sum=100)
        self.payment = Payment.objects.create(bill=self.bill, period=self.period, sum=50)

    def test_Meilahti(self):
        result = self.admin.Meilahti(self.period)
        self.assertEqual(result, 50)

    def test_Lapinlahti(self):
        result = self.admin.Lapinlahti(self.period)
        self.assertEqual(result, None)

    def test_download_csv(self):
        request = None
        queryset = Period.objects.filter(year=timezone.now().year, month=1)
        response = self.admin.download_csv(request, queryset)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Disposition'], 'attachment; filename=resko_{}.csv'.format(self.period))