from django.contrib import admin
from django.contrib.admin import SimpleListFilter

from django.db.models import Sum, Subquery
import locale
locale.setlocale(locale.LC_ALL,'fi_FI')
from decimal import Decimal
from datetime import datetime

# Register your models here.
from app.models import Bill, Period, Payment, PaymentFile #, BillsFile

admin.autodiscover()
admin.site.enable_nav_sidebar = True


class PayInline(admin.TabularInline):
    """
    A class representing a tabular inline for Payment model in Django admin.

    Attributes:
    -----------
    model : Payment
        The Payment model to be used in the inline.
    """

    model = Payment

    def get_queryset(self, request):
        """
        Returns a QuerySet of Payment objects filtered by the current year.

        Parameters:
        -----------
        request : HttpRequest
            The current request.

        Returns:
        --------
        QuerySet
            A QuerySet of Payment objects filtered by the current year.
        """
        qs = super().get_queryset(request)
        return qs.filter(period__year = datetime.now().year)

    def get_extra(self, request, obj=None, **kwargs):
        """
        Returns the number of extra Payment forms to display.

        Parameters:
        -----------
        request : HttpRequest
            The current request.
        obj : Payment, optional
            The Payment object being edited, by default None.

        Returns:
        --------
        int
            The number of extra Payment forms to display.
        """
        extra = 0

        return extra

class BillFilter(SimpleListFilter):
    """
    A filter for Bills in the admin panel that allows filtering by whether they have ended or not.
    """
    title = "Päättyneet"
    parameter_name = "end"

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples representing the available choices for the filter.
        """
        return ((datetime.now().year, datetime.now().year),
        (datetime.now().year-1, datetime.now().year-1)
        )

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the selected filter value.
        """
        print(str(datetime.now().year - 1))
        if self.value() == str(datetime.now().year - 1):
            print('end')
            return queryset.filter(end__year = datetime.now().year - 1)
        return queryset.filter(end__year = datetime.now().year) | queryset.filter(end__year = None)

class BillAdmin(admin.ModelAdmin):
    """
    A custom ModelAdmin class for managing Bill objects in the admin panel.

    Attributes:
    - search_fields (tuple): A tuple of fields to search for when using the search bar in the admin panel.
    - list_display (tuple): A tuple of fields to display in the list view of Bill objects in the admin panel.
    - ordering (tuple): A tuple of fields to use for ordering Bill objects in the admin panel.
    - list_filter (tuple): A tuple of fields to use for filtering Bill objects in the admin panel.
    - inlines (list): A list of inline classes to use for editing related objects in the admin panel.
    - list_editable (tuple): A tuple of fields that can be edited directly in the list view of Bill objects in the admin panel.
    - readonly_fields (list): A list of fields that are read-only in the admin panel.
    - fieldsets (tuple): A tuple of fieldsets to use for grouping fields in the admin panel.

    Methods:
    - debt: A method that returns the cumulative debt of a Bill object.
    - cum_pay: A method that returns the cumulative payment of a Bill object.
    """
    search_fields = ('name','ref','note')
    list_display = ('name','site','room','sum','cum_pay','start','end')
    ordering = ('name','ref','site')
    list_filter = ('site',BillFilter)
    inlines = [PayInline]
    list_editable = ('start','end')
    readonly_fields = ['debt']
    # fields = (('name','email'),('sum','ref'),'note',('site','room'),('start','end'),'debt','comment')
    fieldsets = ((None, {
            'fields': (('name','email'),('sum','ref'),'debt','note',('site','room'),('start_debt','start','end'))
        }),
            ('HUOM', {
                'classes': ('collapse',),
                'fields': ('comment',),
            }),
        )

    def debt(self, obj):
        return obj.cum_pay()

    def cum_pay(self, obj):
        return obj.cum_pay()

    debt.short_description = 'Vuokravelka'
    cum_pay.short_description = 'Velka'


admin.site.register(Bill, BillAdmin)



class PeriodAdmin(admin.ModelAdmin):
    """
    Admin class for managing Period model in the Django admin site.
    """
    search_fields = ('year','month')
    list_display = ('year','month','Meilahti','Lapinlahti','Vuosaari')
    ordering = ('year','month')
    list_filter = ('year','month')

    def Vuosaari(self, obj):
        """
        Returns the sum of payments made for Vuosaari the given period.
        """
        try:
            period_cum = locale.format('%.2f',Payment.objects.filter(period=obj, site='Vuosaari').aggregate(Sum('sum'))['sum__sum'])
            return period_cum
        except:
            return 

    def Lapinlahti(self, obj):
        """
        Returns the sum of payments made for  Lapinlahti  for the given period.
        """
        try:
            period_cum = locale.format('%.2f',Payment.objects.filter(period=obj, site='Lapinlahti').aggregate(Sum('sum'))['sum__sum'])
            return period_cum
        except:
            return 

    def Meilahti(self, obj):
        """
        Returns the sum of payments made for Meilahti for the given period.
        """
        try:
            period_cum = locale.format('%.2f',Payment.objects.filter(period=obj, site='Meilahti').aggregate(Sum('sum'))['sum__sum'])
            return period_cum
        except:
            return 

    def download_resko(self, request, queryset):
        """
        Action method for downloading a xls report of bills and payments for the selected periods.
        """
        import pandas as pd
        import csv
        from django.http import HttpResponse
        from io import StringIO, BytesIO
        
        def remove_exponent(num):
            """
            Helper function for removing the exponent from a Decimal number.
            """
            if num:
                return num.to_integral() if num == num.to_integral() else num.normalize()
            else:
                return 0

        
        year = datetime.now().year
        month = datetime.now().month
        lp_table = []
        vt_table = []
        mt_table = []
        none_table = [] 
        header = ['Huone/Nimi']
        for h in queryset:
            header.append(f"{h.month}/{h.year}")    
            
        bills = Bill.objects.filter(start__lt=queryset.first()).order_by('site','room')
        for bill in bills:
            row1 = []
            row2 = []
            row1.append(f"{bill.room} {bill.name}")
            row2.append(f"{bill.cum_pay()} €")
            for m in queryset:
                if bill.start and bill.start.month <= m.month:
                    row1.append(f"-{bill.sum} €")
                else:
                    row1.append(0)
            for p in queryset:
                pay = Payment.objects.filter(bill=bill, period=p).aggregate(Sum('sum'))
                if pay['sum__sum']:    
                    row2.append(f"+ {locale.format('%.2f', pay['sum__sum'])} €") #locale.format('%.2f', cum)
                else:
                    row2.append(0)
            if bill.site == 'Lapinlahti':
                lp_table.append(row1)
                lp_table.append(row2)
            elif bill.site == 'Vuosaari':
                vt_table.append(row1)
                vt_table.append(row2)
            elif bill.site == 'Meilahti':
                mt_table.append(row1)
                mt_table.append(row2)
            elif bill.site == 'None':
                none_table.append(row1)
                none_table.append(row2)

        lp_df = pd.DataFrame(lp_table, columns=header)
        vt_df = pd.DataFrame(vt_table, columns=header)
        mt_df = pd.DataFrame(mt_table, columns=header)
        none_df = pd.DataFrame(none_table, columns=header)  

        f = BytesIO()
        with pd.ExcelWriter(f, engine='openpyxl') as writer:
            lp_df.to_excel(writer, sheet_name='Lapinlahti', index=False)
            vt_df.to_excel(writer, sheet_name='Vuosaari', index=False)
            mt_df.to_excel(writer, sheet_name='Meilahti', index=False)
            none_df.to_excel(writer, sheet_name='None', index=False)

        f.seek(0)   
        response = HttpResponse(f, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        response['Content-Disposition'] = f'attachment; filename=resko_{year}.xlsx'

        return response


    actions = [download_resko]
    download_resko.short_description = "Lataa resko"


admin.site.register(Period, PeriodAdmin)

class PayAdmin(admin.ModelAdmin):
    """
    Admin class for managing payments.

    Attributes:
    - search_fields: A tuple of fields to search for when using the search bar.
    - list_display: A tuple of fields to display in the list view.
    - ordering: A tuple of fields to order the list view by.
    - list_filter: A tuple of fields to filter the list view by.
    - fields: A tuple of fieldsets to display in the detail view.
    - autocomplete_fields: A list of fields to enable autocomplete for.
    - actions: A list of custom actions to add to the admin page.
    """

    search_fields = ('date','bill__name')
    list_display = ('date','bill','sum','period')
    ordering = ('date','bill','period')
    list_filter = ('period','site')
    # list_editable = ('bill','period')
    fields =(('period','date'),('bill','sum'),'note')
    autocomplete_fields = ['bill']

    def pay2bill(self, request, queryset):
        """
        Custom action to update payments with corresponding bills.

        Args:
        - self: The PayAdmin instance.
        - request: The current request.
        - queryset: The selected payments to update.

        Returns:
        - None.
        """
        for pay in queryset:
            if pay.bill == None:
                for word in pay.note.split(' '):
                    bill_obj = Bill.objects.filter(ref__icontains=word.lstrip('0').replace(' ','')) | Bill.objects.filter(name__icontains=word) | Bill.objects.filter(note__icontains=word )
                    if bill_obj.exists():
                        pay.bill = bill_obj[0]
                        pay.save()
                        if pay.note.split(': IBAN-maksu viitteellä ')[1] not in bill_obj.note.spilt(' '):
                            bill_obj.update(note = '% %'.format(bill_obj.note, pay.note.split(': IBAN-maksu viitteellä ')[1]))

    actions= [pay2bill]
    pay2bill.short_description = 'Päivitä maksut'

admin.site.register(Payment, PayAdmin)


class PaymentFileAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'period', 'timestamp')
    fields = ('period','file')

admin.site.register(PaymentFile, PaymentFileAdmin)
