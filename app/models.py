import json
import sys
import csv
import re
from io import StringIO, BytesIO
from django.db import models
from datetime import datetime
from decimal import Decimal
from django.db.models import Sum
import locale
locale.setlocale(locale.LC_ALL,'fi_FI')
import pandas as pd

# Tilajakamo rental reskontra

SITES = (('Lapinlahti','Lapinlahti'),
        ('Vuosaari','Vuosaari'),
        ('Meilahti','Meilahti'),
        ('None','None')
    )

class Period(models.Model):
    """Django data model rental Period"""
    id = models.AutoField(primary_key=True)
    year = models.IntegerField(default=datetime.now().year)
    month = models.IntegerField(default=datetime.now().month-1)

    class Meta:
        verbose_name = 'Jakso'
        verbose_name_plural = 'Reskot'

    def __str__(self):
        return str("{}/{}".format(self.month, self.year))


class Bill(models.Model):
    """Django data model for rental bill.

    Attributes:
        name (CharField): The name of the bill.
        note (CharField): A note for the bill.
        ref (CharField): The reference for the bill.
        sum (DecimalField): The rental amount for the bill.
        vat (BooleanField): Whether the bill includes VAT.
        site (CharField): The rental site for the bill.
        start (ForeignKey): The start period for the bill.
        end (ForeignKey): The end period for the bill.
        room (CharField): The rental room for the bill.
        email (CharField): The email for the bill.
        comment (TextField): A comment for the bill.
    """
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200, verbose_name='Saaja')
    note = models.CharField(max_length=200, null=True, blank=True)
    ref = models.CharField(max_length=200, null=True, blank=True, verbose_name='Lasku')
    sum = models.DecimalField(decimal_places=2, max_digits=8, verbose_name="Vuokra")
    start_debt = models.DecimalField(decimal_places=2, max_digits=8, null=True, blank=True, verbose_name="Alkuvelka (-)",default=0.0)
    vat = models.BooleanField(default=False)
    site = models.CharField(max_length=100, choices=SITES, blank=True, null=True, verbose_name='Kohde')
    start = models.ForeignKey(Period, on_delete=models.CASCADE, null=True, blank=True, related_name="start_period")
    end = models.ForeignKey(Period, on_delete=models.CASCADE, null=True, blank=True, related_name="end_period")
    room = models.CharField(max_length=200, null=True, blank=True, verbose_name='Huone')
    email = models.CharField(max_length=200, null=True, blank=True)
    comment = models.TextField(null=True, blank=True, verbose_name="Aihe")

    #     # Methods
    def cum_pay(self):
            """Calculate the cumulative payment for the bill.

            This method calculates the cumulative payment for the bill by summing up all the payments made for the bill and subtracting the rent amount. The rent amount is calculated based on the start and end dates of the bill, and the monthly rent amount specified in the bill. If the site of the bill is 'Lapinlahti', an additional discount is applied to the rent amount.

            Returns:
                str: The cumulative payment for the bill, formatted as a string with two decimal places.
            """
            
            cum = Payment.objects.filter(bill=self).aggregate(Sum('sum'))['sum__sum']
            start = 1
            rent = 0
            if self.start and self.start.month > 1:
                start = self.start.month
            if self.end:
                end = self.end.month
            else:
                end = datetime.now().month

            if self.sum and start < datetime.now().month:
                if self.start_debt:
                    paid = self.start_debt + (self.sum * (end - start + 1))
                else:
                    paid = self.sum * (end - start + 1) 

            if cum:
                return locale.format('%.2f', (cum - paid))

            else:
                return locale.format('%.2f', (0 - paid))


    @property
    def bills_cum(self):
        return Payment.objects.filter(bill=self.pk).aggregate(Sum('sum'))['sum__sum']

    bills_cum = property(bills_cum)

    class Meta:
        verbose_name = 'Vuokralasku'
        verbose_name_plural = 'Vuokralaskut'

    def __str__(self):
        return str(self.name)

class BillsFile(models.Model):
    """
    A model representing a file containing bill information.

    Attributes:
        file (FileField): The bill file to be uploaded.
        timestamp (DateTimeField): The timestamp of when the file was uploaded.
    """

    file = models.FileField(verbose_name='Tiedosto')
    timestamp = models.DateTimeField(default=datetime.now, blank=True, verbose_name='Aikaleima', primary_key=True)

    def save(self, *args, **kwargs):
        """
        Overrides the default save method to read and parse the uploaded bill file.

        Returns:
            None
        """
        

        super(BillsFile, self).save(*args, **kwargs)
        filename = self.file.url

    class Meta:
        verbose_name = 'Lasku'
        verbose_name_plural = 'Laskut'

    def __str__(self):
        """
        Returns a string representation of the file.

        Returns:
            str: The name of the file.
        """
        return str(self.file)

class Payment(models.Model):
    """Django data model for rental payments referencing Bills and Periods.

    Attributes:
        period (Period): The rental period associated with the payment.
        bill (Bill): The bill associated with the payment.
        sum (Decimal): The amount paid.
        date (str): The date the payment was made.
        site (str): The rental site associated with the payment.
        note (str): Additional notes about the payment.
    """
    id = models.AutoField(primary_key=True)
    period = models.ForeignKey(Period, on_delete=models.CASCADE, null=True, blank=True, verbose_name='Kausi')
    bill = models.ForeignKey(Bill, on_delete=models.CASCADE, null=True, blank=True, verbose_name='Lasku')
    sum = models.DecimalField(decimal_places=2, max_digits=8, verbose_name='Summa')
    date = models.DateField('Maksupäivä', default=datetime.now, blank=True) #HUOM maksupäiviä voi olla listassa monta
    site = models.CharField(max_length=100, choices=SITES, blank=True, null=True, verbose_name='Kohde')
    note = models.CharField(max_length=200, null=True, blank=True)

    class Meta:
        verbose_name = 'Maksu'
        verbose_name_plural = 'Maksut'

    def __str__(self):
        return str(self.bill)


class PaymentFile(models.Model):

    """
    A model representing a payment file uploaded by a user.
    """
    id = models.AutoField(primary_key=True)
    file = models.FileField(verbose_name='Tiliote')
    timestamp = models.DateTimeField(default=datetime.now, blank=True, verbose_name='Aikaleima')
    period = models.ForeignKey(Period, on_delete=models.CASCADE, null=True, verbose_name='Jakso')

    # class Bill:
    #     def lookup_bill(self, res):
    #         """
    #         A method that looks up a bill based on the given parameters.

    #         Args:
    #         - res: A dictionary containing the parameters to look up the bill.

    #         Returns:
    #         - A Bill object if found, otherwise None.
    #         """

    #         if res['Viite']:
    #             bill_obj = Bill.objects.filter(ref__icontains=res['Viite'].lstrip('0').replace(" ", ""))
    #             if bill_obj:
    #                 return bill_obj[0]

    #         if res['Kuvaus']:
    #             for word in res['Kuvaus'].split(' '):
    #                 if word not in ['IBAN-maksu','viitteellä']:
    #                     bill_obj = Bill.objects.filter(ref__icontains = word) | Bill.objects.filter(name__icontains = word) | Bill.objects.filter(note__icontains = word)
    #                     if bill_obj:
    #                         print(res['Kuvaus'])
    #                         return bill_obj[0]


    #         if res['Vastapuoli']:
    #             bill_obj = Bill.objects.filter(name__icontains = res['Vastapuoli'])
    #             if bill_obj:
    #                 return bill_obj[0]

    #         return None

    def save(self, *args, **kwargs):
            """
            Saves the payment file and creates Payment objects based on the file's content.

            Reads the file, parses its content, and creates Payment objects based on the parsed data.
            The Payment objects are associated with a period and a bill (if applicable), and are saved to the database.

            Args:
                *args: Variable length argument list.
                **kwargs: Arbitrary keyword arguments.

            Returns:
                None
            """
            # use Pandas to read excel file
            f = self.file.read()
            df = pd.read_excel(BytesIO(f), skiprows=7)
            rows = df.values.tolist()

            line = ['Numero', 'Saaja', 'Aihe', 'Luokka', 'Laskutuspäivä', 'Eräpäivä', 'Maksupäivä', 'Tila', 'Veroton summa', 'ALV Summa', 'Laskutettu/hyvitetty määrä', 'Maksettu summa']

            for row in rows:
                res = {}
                for i, key in enumerate(line):
                    res[key] = row[i]
                if res['Tila'] not in ['Hyvityslasku','Hyvitetty','Mitätöity']:
                    if res['Luokka'].startswith("Vuokratulot"):
                        if res['Aihe'].startswith('Vuosaarentie') or res['Aihe'].startswith('VT') or res['Aihe'].startswith('Vuokra VT'):
                            kohde = 'Vuosaari'
                        elif res['Aihe'].startswith('Tilavuokra'):
                            kohde = 'Lapinlahti'
                        elif res['Aihe'].startswith('Tukholmankatu'):
                            kohde = 'Meilahti'
                        else:
                            kohde = 'None'
                        bill = Bill.objects.get_or_create(name=res['Saaja'], sum=res['Laskutettu/hyvitetty määrä'])
                        bill[0].ref = res['Numero']
                        bill[0].note = res['Aihe']
                        bill[0].site = kohde
                        try:
                            huone = res['Aihe'].split(' huone ')[1].split(' ')[0]
                        except:
                            huone = res['Aihe']
                        bill[0].room = huone
                        bill[0].start = self.period
                        bill[0].save()

                        if res['Tila'] == 'Erääntynyt':
                            paid_date = res['Eräpäivä']
                        else:
                            paid_date = res['Maksupäivä']

                        try:
                            paid_dates = paid_date.split(',')
                        except:
                            paid_dates = [paid_date]
                        if len(paid_dates) > 1:
                            for date in paid_dates:
                                period = Period.objects.filter(year=datetime.strptime(date, '%d.%m.%Y').year, month=datetime.strptime(date, '%d.%m.%Y').month) or [self.period]
                                pay = Payment.objects.create(period=period[0] or self.period,
                                bill=bill[0],
                                sum=res['Laskutettu/hyvitetty määrä'],
                                site=kohde,
                                date=datetime.strptime(date, '%d.%m.%Y'),
                                note=f"{res['Saaja']} {res['Numero']} {res['Tila']} {res['Eräpäivä']} {res['Laskutettu/hyvitetty määrä']}")                    

                        else:
                            period = Period.objects.filter(year=datetime.strptime(paid_date, '%d.%m.%Y').year, month=datetime.strptime(paid_date, '%d.%m.%Y').month) or [self.period]
                            pay = Payment.objects.create(period=period[0],
                                bill=bill[0],
                                sum=res['Laskutettu/hyvitetty määrä'],
                                site=kohde,
                                date=datetime.strptime(paid_date, '%d.%m.%Y'),
                                note=f"{res['Saaja']} {res['Numero']} {res['Tila']} {res['Eräpäivä']} {res['Laskutettu/hyvitetty määrä']}")                    
            

            super(PaymentFile, self).save(*args, **kwargs)
            filename = self.file.url

    def __str__(self):
        """
        A method that returns the string representation of the PaymentFile object.
        """

        return str(self.file)

    class Meta:
        """
        A class that contains metadata for the PaymentFile model.
        """

        verbose_name = 'Laskutusraportti'
        verbose_name_plural = 'Laskutusraportit'
