from django.test import TestCase
from django.core.files.uploadedfile import SimpleUploadedFile
from django.utils import timezone
from app.models import PaymentFile, Period, Bill, Payment
from decimal import Decimal
from datetime import datetime
from io import StringIO
import csv

class PaymentFileTestCase(TestCase):
    def setUp(self):
        self.period = Period.objects.create(year=timezone.now().year, month=1)
        self.bill = Bill.objects.create(site='Meilahti', name='Test Bill', sum=100)
        self.file = SimpleUploadedFile("file.txt", b"file_content")
        self.payment_file = PaymentFile.objects.create(file=self.file, period=self.period)

    def test_save(self):
        # create a CSV string with payment data
        csv_string = 'Päiväys;Summa;Valuutta;Vastapuoli;Kuvaus;Viite;Viesti;Arkistointitunnus\n'
        csv_string += '2022-01-01;50.00;EUR;Test Bill;Test payment;123456789;Test message;Test archive\n'
        csv_file = SimpleUploadedFile("test.csv", csv_string.encode('utf-8'))

        # set the payment file's file field to the CSV file
        self.payment_file.file = csv_file
        self.payment_file.save()

        # check that a payment object was created with the correct data
        payment = Payment.objects.get(bill=self.bill)
        self.assertEqual(payment.period, self.period)
        self.assertEqual(payment.date, datetime.strptime('2022-01-01', '%Y-%m-%d').date())
        self.assertEqual(payment.bill, self.bill)
        self.assertEqual(payment.sum, Decimal('50.00'))
        self.assertEqual(payment.site, 'Meilahti')
        self.assertEqual(payment.note, 'Test Bill: Test payment')

    def test_lookup_bill(self):
        # create a bill object
        bill = Bill.objects.create(site='Test Site', name='Test Bill', sum=100)

        # create a dictionary with payment data
        payment_data = {
            'Päiväys': '2022-01-01',
            'Summa': '50.00',
            'Valuutta': 'EUR',
            'Vastapuoli': 'Test Site',
            'Kuvaus': 'Test payment',
            'Viite': '123456789',
            'Viesti': 'Test message',
            'Arkistointitunnus': 'Test archive'
        }

        # call the lookup_bill method and check that it returns the correct bill object
        payment_file = PaymentFile()
        bill_obj = payment_file.lookup_bill(payment_data)
        self.assertEqual(bill_obj, bill)