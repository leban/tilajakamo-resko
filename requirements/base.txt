Django==3.0.6
django-crispy-forms==1.9.1
django-environ==0.4.5
django-extensions==2.2.9
# mysqlclient==1.4.6
pip==24.0 
# Pillow==7.1.2
# psycopg2-binary
pytz
six==1.15.0
sorl-thumbnail==12.6.3
django-admin-interface
pandas