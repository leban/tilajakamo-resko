from django.contrib import admin
from django.db.models import Sum
# Register your models here.

from app.models import Bill, Period, Payment, BillsFile, PaymentFile

class BillAdmin(admin.ModelAdmin):
    search_fields = ('name','ref','note')
    list_display = ('name','sum','ref','site')
    ordering = ('name','ref')
    list_filter = ('site',)

admin.site.register(Bill, BillAdmin)

class PeriodAdmin(admin.ModelAdmin):
    search_fields = ('year','month')
    list_display = ('year','month')
    ordering = ('year','month')

    def download_csv(self, request, queryset):
        import csv
        from django.http import HttpResponse
        from io import StringIO

        def remove_exponent(num):
            return num.to_integral() if num == num.to_integral() else num.normalize()

        f = StringIO()
        writer = csv.writer(f)
        for period in queryset:
            writer.writerow(["Kohde","Lasku", "Maksu {}".format(period), "Kertymä", "Vuokra","Velka"])
            # laske maksut vuoden alusta
            # laske velka vuoden alusta
            #
            bills = Bill.objects.all()
            for bill in bills:
                cum = 0
                if bill.start:
                    cum = (period.month - bill.start.month) * bill.sum
                else:
                    cum = bill.sum * period.month
                sum = 0
                sumup = Payment.objects.filter(bill=bill).aggregate(Sum('sum'))
                if sum:
                    sum = remove_exponent(sumup['sum__sum'])
                pays = Payment.objects.filter(bill=bill, period=period)
                pay = 0
                if pays:
                    pay = pays[0].sum
                writer.writerow([bill.site, bill.name, pay, cum, sum, sum - cum])

        f.seek(0)
        response = HttpResponse(f, content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename=resko_{}.csv'.format(period)
        return response

    actions = [download_csv]
    download_csv.short_description = "Lataa raportti"

admin.site.register(Period, PeriodAdmin)

class PayAdmin(admin.ModelAdmin):
    search_fields = ('date','bill__name')
    list_display = ('date','bill','sum','period')
    ordering = ('date','bill','period')
    list_filter = ('period','site')


admin.site.register(Payment, PayAdmin)

class BillsFileAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'timestamp')

admin.site.register(BillsFile, BillsFileAdmin)

class PaymentFileAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'period', 'timestamp')

admin.site.register(PaymentFile, PaymentFileAdmin)
